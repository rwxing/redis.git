package com.zhu.redis.dao;

import java.util.List;

import com.zhu.redis.entity.Area;

/**
 * 对区域操作的dao�?
 * @author zhu
 *
 */
public interface AreaDao {
	/**
	 * 查询�?有区�?
	 * @return
	 */
	List<Area> queryArea();

}
